# 90-seats.rules
# 2018-10-10 - udev rules for multiseat, originally under slackware
# 2019-03-25 - udev update based on https://forums.gentoo.org/viewtopic-p-7433934.html
# 2020-04-19 - Update comments and device assignments, adopted for ubuntu

# To quickly review tags:
# for f in /dev/input/* ;  do echo $f; udevadm info -q all -n $f | grep tag ; done

## Seat 0
#
# Given everything is assigned by systemd/logind to seat0 by default this may
# not be necessary, but likely prudent.
# Default all input devices to seat0
SUBSYSTEM=="input", ENV{ID_INPUT.tags}="input_default", TAG="input_default"
#SUBSYSTEM=="input", ENV{ID_SEAT}="seat0"

# Adding symlinks for input devices can make the contents of /dev/input more 
# coherent at a glance, and also allows for easier device assignment in the
# xorg configuration file.

# Keyboard, TypeMatrix 3030
SUBSYSTEM=="input", DEVPATH=="/devices/platform/i8042/serio0/input/input0/event0", SYMLINK+="input/typematrix"
# Mouse, Steel Series, 1038:1369
KERNEL=="event*", SUBSYSTEM=="input", ATTRS{product}=="Sensei Raw Gaming Mouse", ATTRS{idVendor}=="1038", ATTRS{idProduct}=="1369", SYMLINK+="input/steelseries%n"
# Headset, Corsair, 8087:1b1d
SUBSYSTEM=="event*", ATTRS{product}=="Corsair Gaming H1500 Headset", ATTRS{idVendor}=="8087", ATTRS{idProduct}=="1b1d", SYMLINK+="input/headset%n" 

## Seat 1
#
# Display controller: Intel Corporation Xeon E3-1200 v2/3rd Gen Core processor Graphics Controller, 8086:0152
DEVPATH=="/devices/pci0000:00/0000:00:02.0", TAG+="graphics1", SUBSYSTEM=="drm", ENV{ID_SEAT}="seat1", TAG+="master-of-seat"
SUBSYSTEM=="graphics", TAG=="graphics1" , ENV{ID_SEAT}="seat1"
#SUBSYSTEM=="drm", TAG+="graphics1", ENV{ID_SEAT}="seat1", TAG+="master-of-seat",
# Mouse, Microsoft_Microsoft®_Comfort_Mouse_4500, 045e:076c
KERNEL=="event*", SUBSYSTEM=="input", ATTRS{idVendor}=="045e", ATTRS{idProduct}=="076c", ENV{ID_SEAT}="seat1", SYMLINK+="input/ms4500%n"
# Keyboard, Elan Microelectronics Corp. ActiveJet K-2024 Multimedia Keyboard, 04f3:0103
SUBSYSTEM=="input", ATTRS{idVendor}=="04f3", ATTRS{idProduct}=="0103", ENV{ID_SEAT}="seat1"


# As borrowed from https://forums.gentoo.org/viewtopic-p-7433934.html

# What is not special seat gets seat0
SUBSYSTEM=="input", TAG=="seat", ENV{ID_SEAT}=="" , ENV{ID_SEAT}="seat0"

# Finally, set all tags accordingly.  
# If ID_SEAT is not explicitly set, set the ID_INPUT to match ID_SEAT
ENV{ID_SEAT}!="", ENV{ID_INPUT.tags}="$env{ID_SEAT}" TAG="$env{ID_SEAT}"
