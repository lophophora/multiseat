# Multiseat Xorg Configuration

## System Information

### System
- Ubuntu 18.04
- 5.3.0-42-lowlatency
- systemd 237
- X.Org X Server 1.19.6
- NVIDIA GeForce GTX 660 (435.21)
- Intel HD Graphics 2500 (i915, version 1.6.0 20190619)
- Two Mice
- Two Keyboards
- Two Monitors


## Launching MultiSeat

### Seat0
`/usr/bin/startx -- :0 vt01 -keeptty -nolisten tcp -config nvidia_xorg.conf -seat seat0`

### Seat1
`/usr/bin/startx -- :1 vt02 -keeptty -nolisten tcp -novtswitch -sharevts -config intel_xorg.conf -seat seat1`

I have no idea if the `-seat seat1` argument is even required, or makes much of a difference.  As far as logind is concerned I am still using `seat0`.
I have tried using `loginctl attach` to create a new seat but all I get is a bunch of rules in `/etc/udev/rules.d`, tagging the devices as seat 1.

Once the Xsession starts I am unable to switch back to `vt01`, likely on account of the `-novtswitch` argument.
Using `loginctl activate 1` restores control to the first seat/session.

## Notes

### Grub and KMS

Kernel modesetting needs to be used in order for the integrated intel graphics to be detected (in this particular configuration).  This results in a blank screen following the grub menu, and requires blindly mashing the keyboard to log in and start an X session.
This is easily avoided with a window manager, but not needed for the present use case.

### Autostart/Autologin

Easiest solution is to use a window manager.

Another alternative is to set up autologin using `/etc/inittab`, and use `.bash_profile` to automatically launch the X session.
```bash
# ~/.bash_profile
if [ -z "$DISPLAY" ] && [ $(tty) == /dev/tty1 ]; then
  startx -- :0 vt01 -keeptty -nolisten tcp -config nvidia_xorg.conf
  logout
fi

if [ -z "$DISPLAY" ] && [ $(tty) == /dev/tty2 ]; then
  startx -- :1 vt02 -keeptty -nolisten tcp -novtswitch -sharevts -config intel_xorg.conf
  logout
fi
```

```bash
# /etc/inittab example, add an entry for each tty concerned.

x2:4:respawn:/sbin/agetty 38400 tty2 linux -a USERNAME

```

Or if using systemd, by modifying the appropriate `getty@.service` file.

## Links

- https://www.freedesktop.org/wiki/Software/systemd/multiseat/
- https://www.x.org/wiki/Development/Documentation/Multiseat/
- https://wiki.ubuntu.com/MultiseatTeam/Instructions
- https://forums.gentoo.org/viewtopic-p-7433934.html
- https://wiki.archlinux.org/index.php/Xorg_multiseat
